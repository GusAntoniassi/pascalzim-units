unit Pzim_String;

interface
uses Pzim_Math;
function intToStr(num : integer):string;

implementation
	
	function intToStr(num : integer):string;
	var divisor, ponteiro : integer;
			divisao : real;
			s : string;
	begin
		ponteiro:= contarDigitosInt(num);
		
		while ponteiro > 0 do
		begin
			// O ponteiro aponta para qual n�mero n�s estamos trabalhando - 1
			ponteiro:= ponteiro-1;
			// Fazemos 10^ponteiro 
			divisor:= int(potenciar(10, ponteiro));
			
			// Com essa divis�o, conseguimos apenas o primeiro n�mero como inteiro e o resto como decimal 
			divisao:= num / divisor;
			
			// Adicionamos o char correspondente ao inteiro obtido com o resto do n�mero
			s:= s + chr(int(divisao) + 48);
			
			// Transformamos a fra��o em um n�mero inteiro novamente para calcul�-lo na pr�xima repeti��o
			num:= int(frac(divisao) * potenciar(10, ponteiro));
		end;
		
		intToStr:= s;
	end;
end.
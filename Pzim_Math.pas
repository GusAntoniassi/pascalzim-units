unit Pzim_Math;
interface
function potenciar(num, base : real):real;    
function contarDigitosInt(num : integer):integer;

implementation
	function potenciar(num, base : real):real;
	begin
	  if num <> 0 then
	  potenciar:= exp(base*ln(num))
	  else
	  potenciar:= 0;
	end;
	
	function contarDigitosInt(num : integer):integer;
	begin
	  if (num = 0) then
	  contarDigitosInt:= 1
	  else
	  // Log10(|num|)+ retorna o número de dígitos daquele número
	  contarDigitosInt:= int(ln(abs(num))/ln(10))+1;
	end;
end.